package com.ttime.configuration;

import java.util.Hashtable;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class LdapCredentailValidation {
	
	private static final Logger LOGGER = Logger.getLogger(LdapCredentailValidation.class.getName());
	
	private String ldapServers;
    private String userSuffix;
    
    public LdapCredentailValidation(String servers, int port, String userSuffix, String ldapRoot)    {
        ldapServers = servers;
        this.userSuffix = userSuffix;
    }
    
    public boolean checkUserCredential(String userName, String password){
        try{        	
        	// Set up the environment for creating the initial context
        	Hashtable<String, String> env = new Hashtable<>();
        	env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        	env.put(Context.SECURITY_PROTOCOL,"ssl");
        	env.put(Context.PROVIDER_URL, ldapServers);

        	// Authenticate using UserName and Password
        	env.put(Context.SECURITY_AUTHENTICATION, "simple");
        	env.put(Context.SECURITY_PRINCIPAL, userName+"@"+userSuffix);
        	env.put(Context.SECURITY_CREDENTIALS, password);
        	DirContext ctx = new InitialDirContext(env);
        	ctx.close();
            return true;
        }
        catch (Exception e){
        	LOGGER.info("Error: " + e.toString());
        }
		return false;
    }
}
