package com.ttime.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ttime.dto.Convert;
import com.ttime.service.DayService;
import com.ttime.service.PersonService;
import com.ttime.service.ProjectRoleService;
import com.ttime.service.StatusService;
import com.ttime.service.UserCredentialsService;
import com.ttime.service.WorkHoursService;


@Configuration
public class AppConfig {
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	
	@Bean 
	public Convert convert() {
		return new Convert();
	}
	
	@Bean
	public PersonService personService() {
		return new PersonService();
	}

	@Bean
	public UserCredentialsService userCredentialsService() {
		return new UserCredentialsService();
	}
	
	@Bean
	public DayService dayService() {
		return new DayService();
	}
	
	@Bean
	public ProjectRoleService projectRoleService() {
		return new ProjectRoleService();
	}
	
	@Bean
	public StatusService statusService() {
		return new StatusService();
	}
	
	@Bean
	public WorkHoursService workHoursService() {
		return new WorkHoursService();
	}
	
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
    	return new BCryptPasswordEncoder();
    }
}
