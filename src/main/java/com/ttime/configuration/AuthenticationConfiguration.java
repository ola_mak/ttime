package com.ttime.configuration;

import static com.ttime.configuration.SecurityConstants.SIGN_UP_URL;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
public class AuthenticationConfiguration extends WebSecurityConfigurerAdapter {
	
	private final UserDetailsService userDetailsService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public AuthenticationConfiguration(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		JWTAuthenticationFilter jwtAuthenticationFilter = new JWTAuthenticationFilter(JWTAuthenticationManager());
		jwtAuthenticationFilter.setFilterProcessesUrl("/api/login");
		
		http.authorizeRequests()
		.antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
		.antMatchers(HttpMethod.DELETE, "/api/user").hasRole("admin")
		.antMatchers( HttpMethod.DELETE, "/api/projectRole").hasRole("admin")
		.antMatchers( HttpMethod.DELETE, "/api/status").hasRole("admin")
//		.antMatchers("/api/projectRoles").hasRole("admin")
//		.antMatchers("/api/statuses").hasRole("admin")
//		.antMatchers("/api/userCreds").hasRole("admin")
//		.antMatchers("/api/pass").hasRole("admin")
//		.antMatchers("/api/role").hasRole("admin")
//		.antMatchers("/api/username").hasRole("admin")
//		.anyRequest().authenticated()
		.anyRequest().permitAll()
		.and()
		.addFilter(jwtAuthenticationFilter)
		.addFilter(new JWTAuthorizationFilter(authenticationManager()));
		http.csrf().disable();
	}
	
	@Override
	public void configure(AuthenticationManagerBuilder authentication) throws Exception {
		authentication.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}
	
	@Bean
	public JWTAuthenticationManager JWTAuthenticationManager() {
		return new JWTAuthenticationManager();
	}
}