package com.ttime.configuration;

import java.util.logging.Logger;

public class LDAPConfiguration {
	
	private static final Logger LOGGER = Logger.getLogger(LDAPConfiguration.class.getName());
	
    static String servers = "ldap://dc1.labs.wmi.amu.edu.pl:636/";
    static String suffix =  "labs.wmi.amu.edu.pl";
    static int port = 636;
    static String root = "DC=labs,DC=wmi,DC=amu,DC=edu,DC=pl";
    
    public static boolean checkUserCredentials(String username, String password) {
    	LOGGER.info("Checking user credentials in LDAP");
    	LdapCredentailValidation lucv = new LdapCredentailValidation(servers, port, suffix, root);
    	System.out.println("Validating credentials, result: ");
    	try {
    		boolean isCredentialCorrect = lucv.checkUserCredential(username, password);
    		LOGGER.info("Are credentials correct: " + isCredentialCorrect);
    		return isCredentialCorrect;
    	} catch (Exception e){
    		LOGGER.warning(e.toString());
            return false;
    	}
    }
}
