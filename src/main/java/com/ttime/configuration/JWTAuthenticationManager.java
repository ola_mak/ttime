package com.ttime.configuration;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.ttime.dto.Convert;
import com.ttime.repository.Address;
import com.ttime.repository.Day;
import com.ttime.repository.Person;
import com.ttime.repository.PersonRepository;
import com.ttime.repository.Role;
import com.ttime.repository.UserCredentials;
import com.ttime.repository.UserCredentialsRepository;
import com.ttime.service.PersonService;

public class JWTAuthenticationManager implements AuthenticationManager{
	
	@Autowired
	PersonService personService;
	@Autowired
	Convert convert;
	@Autowired
	PersonRepository personRepository;
	@Autowired
	UserCredentialsRepository userCredentialsRepository;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();
		boolean authenticated;
		
		UserCredentials userCredentials = userCredentialsRepository.findByUsername(username);
		
		if (userCredentials == null) {
			if (LDAPConfiguration.checkUserCredentials(username, password)) {
				createAccountForNewLdapUser(username);
				authenticated = true;
			} else {
				authenticated = false;
			}
	    } else {
	    	if (userCredentials.isLdap()) {
		        authenticated = LDAPConfiguration.checkUserCredentials(username, password);
			} else {
				authenticated = personService.obtainUser(username, password);
			}
	    }
		
		if (authenticated) {
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), 
					authentication.getCredentials(), 
					getAuthority(username));
			return authToken; 
		} else {
			throw new BadCredentialsException("Invalid username or password");
		}		
	}

	private void createAccountForNewLdapUser(String username) {
		Person person = new Person(new HashSet<Day>(), new Address("", "", "", "", ""), 
				new UserCredentials(Role.user, username, null, true));
		personRepository.save(person);
	}
	
	private Set<SimpleGrantedAuthority> getAuthority(String username) {
		UserCredentials userCredentials = userCredentialsRepository.findByUsername(username);
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + userCredentials.getRole()));
	
		return authorities;
	}
}
