package com.ttime.configuration;

import static com.ttime.configuration.SecurityConstants.AUTHORITIES_KEY;
import static com.ttime.configuration.SecurityConstants.EXPIRATION_TIME;
import static com.ttime.configuration.SecurityConstants.HEADER_STRING;
import static com.ttime.configuration.SecurityConstants.SECRET;
import static com.ttime.configuration.SecurityConstants.TOKEN_PREFIX;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ttime.repository.UserCredentials;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private JWTAuthenticationManager JWTAuthenticationManager;

	public JWTAuthenticationFilter(JWTAuthenticationManager JWTAuthenticationManager) {
		this.JWTAuthenticationManager = JWTAuthenticationManager;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		try {
			UserCredentials creds = new ObjectMapper()
	                    .readValue(request.getInputStream(), UserCredentials.class);
	            return JWTAuthenticationManager.authenticate(
	                    new UsernamePasswordAuthenticationToken(
	                            creds.getUsername(),
	                            creds.getPassword(), 
	                            new ArrayList<>())
			);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, 
			FilterChain chain, Authentication authentication) {
		
		final String authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
		
		String token = Jwts.builder()
				.setSubject((String) (authentication.getPrincipal()))
				.claim(AUTHORITIES_KEY, authorities)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SECRET)
				.compact();
		response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
	}
	
}
