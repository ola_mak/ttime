package com.ttime.dto;

import java.util.List;

public class ProjectRoleDTO {
	
	private List<String> roles;

	public ProjectRoleDTO(List<String> roles) {
		this.roles = roles;
	}

	public ProjectRoleDTO() {
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
}
