package com.ttime.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ttime.repository.ProjectRole;

public class PersonDTO{
	
	private int id;
	private String name;
	private String surname;
	private String avatarURL;
	private ProjectRole role;
	private List<DayGetDTO> days;	
	private AddressDTO address;
	private Date dateOfBirth;
	private String phoneNumber;
	private String emailAddress;
	private UserCredentialsDTO userCredentials;

	
	public PersonDTO(int id, String name, String surname, String avatarURL, ProjectRole role, List<DayGetDTO> days, 
			AddressDTO address, Date dateOfBirth, String phoneNumber, String emailAddress, UserCredentialsDTO userCredentials) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.avatarURL = avatarURL;
		this.role = role;
		this.days = days;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
		this.userCredentials = userCredentials;
	}

	public PersonDTO() {
	}
	
	public PersonDTO(PersonDTO that) {
	    this(that.getId(), that.getName(), that.getSurname(), that.getAvatarURL(), that.getRole(), 
	    		new ArrayList<>(that.getDays()), that.getAddress(), that.getDateOfBirth(), that.getPhoneNumber(),
	    		that.getEmailAddress(), that.userCredentials);
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getAvatarURL() {
		return avatarURL;
	}
	
	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}

	public ProjectRole getRole() {
		return role;
	}

	public void setRole(ProjectRole role) {
		this.role = role;
	}

	public List<DayGetDTO> getDays() {
		return days;
	}
	
	public void setDays(List<DayGetDTO> days) {
		this.days = days;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public UserCredentialsDTO getUserCredentials() {
		return userCredentials;
	}

	public void setUserCredentials(UserCredentialsDTO userCredentials) {
		this.userCredentials = userCredentials;
	}
	
	
}