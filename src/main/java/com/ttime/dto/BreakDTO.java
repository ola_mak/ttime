package com.ttime.dto;

import java.util.Date;

public class BreakDTO {
	
	private Date fromHour;
	private Date toHour;
	
	public BreakDTO(Date fromHour, Date toHour) {
		this.fromHour = fromHour;
		this.toHour = toHour;
	}
	
	public BreakDTO() {
	}
	
	public Date getFromHour() {
		return fromHour;
	}

	public void setFromHour(Date fromHour) {
		this.fromHour = fromHour;
	}

	public Date getToHour() {
		return toHour;
	}

	public void setToHour(Date toHour) {
		this.toHour = toHour;
	}

}