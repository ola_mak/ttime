package com.ttime.dto;

public class StatusUpdateDTO {
	
	private int id;
	private String status;
	
	public StatusUpdateDTO(int id, String status) {
		this.id = id;
		this.status = status;
	}
	
	public StatusUpdateDTO(String status) {
		this.status = status;
	}

	public StatusUpdateDTO() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
