package com.ttime.dto;

import java.util.Date;
import java.util.List;

public class DayDTO implements Comparable<DayDTO>{
	
	private Date date;
	private List<WorkHoursDTO> workHours;
	private List<BreakDTO> breaks;
	
	public DayDTO(Date date, List<WorkHoursDTO> workHours, List<BreakDTO> breaks) {
		this.date = date;
		this.workHours = workHours;
		this.breaks = breaks;
	}

	public DayDTO() {
	}
	
	public List<WorkHoursDTO> getWorkHours() {
		return workHours;
	}

	public void setWorkHours(List<WorkHoursDTO> workHours) {
		this.workHours = workHours;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<BreakDTO> getBreaks() {
		return breaks;
	}

	public void setBreaks(List<BreakDTO> breaks) {
		this.breaks = breaks;
	}

	@Override
	public int compareTo(DayDTO compareDayDTO) {
		return getDate().compareTo(compareDayDTO.getDate());
	}
	
}