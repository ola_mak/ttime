package com.ttime.dto;

import com.ttime.repository.Role;

public class UpdateUserCredentialsDTO {
	
	private Role role;
	private String username;
	private String password;
	private boolean ldap;
	
	public UpdateUserCredentialsDTO() {
	}

	public UpdateUserCredentialsDTO(Role role, String username, String password, boolean ldap) {
		this.role = role;
		this.username = username;
		this.password = password;
		this.ldap = ldap;
	}

	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLdap() {
		return ldap;
	}

	public void setLdap(boolean ldap) {
		this.ldap = ldap;
	}
	
}
