package com.ttime.dto;

import com.ttime.repository.Role;

public class UserCredentialsAddUserDTO {
	
	private Role role;
	private String username;
	private String password;
	private boolean ldap;
	
	public UserCredentialsAddUserDTO() {
	}

	public UserCredentialsAddUserDTO(Role role, String username, String password, boolean ldap) {
		super();
		this.role = role;
		this.username = username;
		this.ldap = ldap;
		this.password = password;
	}

	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLdap() {
		return ldap;
	}

	public void setLdap(boolean ldap) {
		this.ldap = ldap;
	}
	
}
