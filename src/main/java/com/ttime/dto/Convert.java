package com.ttime.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.expression.ParseException;

import com.ttime.repository.Address;
import com.ttime.repository.Break;
import com.ttime.repository.Day;
import com.ttime.repository.Person;
import com.ttime.repository.ProjectRole;
import com.ttime.repository.Status;
import com.ttime.repository.UserCredentials;
//import com.ttime.repository.UserCredentials;
import com.ttime.repository.WorkHours;

public class Convert {
	
    private static final ModelMapper modelMapper = new ModelMapper();
	
    public Address convertAddressToEntity(AddressDTO addressDTO) throws ParseException {
		Address address = modelMapper.map(addressDTO, Address.class);
	  
	    return address;
	}
	
	public AddressDTO convertAddressToDto(Address address) {
		AddressDTO addressDTO = modelMapper.map(address, AddressDTO.class);
		return addressDTO;
	}
	
	public Set<WorkHours> convertWorkHoursToEntity(List<WorkHoursDTO> workHoursDTO) {
		Set<WorkHours> workHours = new HashSet<>();
		for (WorkHoursDTO w : workHoursDTO) {
			workHours.add(convertWorkHoursToEntity(w));
		}
		return workHours;
	}
	
	public WorkHours convertWorkHoursToEntity(WorkHoursDTO workHoursDTO) throws ParseException {
		WorkHours workHours = modelMapper.map(workHoursDTO, WorkHours.class);
	    return workHours;
	}
	
	public Status convertStatusToEntity(StatusUpdateDTO statusUpdateDTO) throws ParseException {
		Status workHours = modelMapper.map(statusUpdateDTO, Status.class);
	    return workHours;
	}
	
	public ProjectRole convertProjectRoleToEntity(ProjectRoleUpdateDTO projectRoleUpdateDTO) throws ParseException {
		ProjectRole projectRole = modelMapper.map(projectRoleUpdateDTO, ProjectRole.class);
	    return projectRole;
	}
	
    public Break convertBreakToEntity(BreakDTO breakDTO) throws ParseException {
    	Break breaks = modelMapper.map(breakDTO, Break.class);
	    return breaks;
	}
    
	public Set<Break> convertBreaksToEntity(List<BreakDTO> breaksDTO) {
		Set<Break> breaks = new HashSet<>();
		for (BreakDTO b : breaksDTO) {
			breaks.add(convertBreakToEntity(b));
		}
		return breaks;
	}
	
	 public Day convertDayToEntity(DayDTO dayDTO) throws ParseException {
		 Day day = modelMapper.map(dayDTO, Day.class);
		    return day;
	}
	
	public Set<Day> convertDaysToEntity(List<DayDTO> daysDTO) {
		Set<Day> days = new HashSet<>();
		for (DayDTO b : daysDTO) {
			days.add(convertDayToEntity(b));
		}
		return days;
	}
	
	public BreakDTO convertBreakToDto(Break breaks) {
		BreakDTO breakDTO = modelMapper.map(breaks, BreakDTO.class);
		return breakDTO;
		
	}

	public DayDTO convertDayToDto(Day day) {
		DayDTO dayDTO = modelMapper.map(day, DayDTO.class);
		return dayDTO;
	}
	
	public PersonDTO convertPersonToDto(Person person) {
		PersonDTO personDTO = modelMapper.map(person, PersonDTO.class);
		return personDTO;
	}
		
	public Person convertPersonToEntity(PersonAddUserDTO personAddUserDTO) throws ParseException {
		Person person = modelMapper.map(personAddUserDTO, Person.class);
	    return person;
	}
	
	public ProjectRole convertProjectRoleToEntity(ProjectRoleDTO projectRoleDTO) throws ParseException {
		ProjectRole projectRole = modelMapper.map(projectRoleDTO, ProjectRole.class);
	    return projectRole;
	}
	
	public ProjectRoleDTO convertProjectRoleToDTO(Iterable<ProjectRole> iterable) {
		ProjectRoleDTO projectRoleDTO = modelMapper.map(iterable, ProjectRoleDTO.class);
		return projectRoleDTO;
	}
	
	public UserCredentials convertUserCredentialsToEntity(UserCredentialsAddUserDTO userCredentialsAddUserDTO) throws ParseException {
		UserCredentials userCredentials = modelMapper.map(userCredentialsAddUserDTO, UserCredentials.class);
	    return userCredentials;
	}
	
}
