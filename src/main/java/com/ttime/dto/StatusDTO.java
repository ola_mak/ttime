package com.ttime.dto;

import java.util.List;

public class StatusDTO {
	
	private List<String> statuses;

	public StatusDTO(List<String> statuses) {
		this.statuses = statuses;
	}

	public StatusDTO() {
	}

	public List<String> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}


}
