package com.ttime.dto;

import com.ttime.repository.Role;

public class UserCredentialsDTO {
	
	private Role role;
	private String username;
	private boolean ldap;
	
	public UserCredentialsDTO() {
	}

	public UserCredentialsDTO(Role role, String username, boolean ldap) {
		super();
		this.role = role;
		this.username = username;
		this.ldap = ldap;
	}

	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isLdap() {
		return ldap;
	}

	public void setLdap(boolean ldap) {
		this.ldap = ldap;
	}
	
}
