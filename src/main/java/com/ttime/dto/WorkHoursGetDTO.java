package com.ttime.dto;

import java.util.Date;

import com.ttime.repository.Status;

public class WorkHoursGetDTO {
	
	private Status status;
	private Date fromHour;
	private Date toHour;
	
	public WorkHoursGetDTO(Status status, Date fromHour, Date toHour) {
		this.fromHour = fromHour;
		this.toHour = toHour;
		this.status = status;
	}
	
	public WorkHoursGetDTO() {
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getFromHour() {
		return fromHour;
	}

	public void setFromHour(Date fromHour) {
		this.fromHour = fromHour;
	}

	public Date getToHour() {
		return toHour;
	}

	public void setToHour(Date toHour) {
		this.toHour = toHour;
	}
	
}
