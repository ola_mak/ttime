package com.ttime.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PersonAddUserDTO {
	
	private String name;
	private String surname;
	private String avatarURL;
	private Integer roleID;
	private List<DayDTO> days;	
	private AddressDTO address;
	private Date dateOfBirth;
	private String phoneNumber;
	private String emailAddress;
	private UserCredentialsAddUserDTO userCredentials;

	
	public PersonAddUserDTO(String name, String surname, String avatarURL, Integer roleID, List<DayDTO> days, 
			AddressDTO address, Date dateOfBirth, String phoneNumber, String emailAddress, UserCredentialsAddUserDTO userCredentials) {
		this.name = name;
		this.surname = surname;
		this.avatarURL = avatarURL;
		this.roleID = roleID;
		this.days = days;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
		this.userCredentials = userCredentials;
	}
	
	public PersonAddUserDTO(String name, String surname, String avatarURL, List<DayDTO> days, 
			AddressDTO address, Date dateOfBirth, String phoneNumber, String emailAddress, UserCredentialsAddUserDTO userCredentials) {
		this.name = name;
		this.surname = surname;
		this.avatarURL = avatarURL;
		this.days = days;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
		this.userCredentials = userCredentials;
	}
	
	public PersonAddUserDTO(UserCredentialsAddUserDTO userCredentials) {
		this.userCredentials = userCredentials;
	}

	public PersonAddUserDTO() {
	}
	
	public PersonAddUserDTO(PersonAddUserDTO that) {
	    this(that.getName(), that.getSurname(), that.getAvatarURL(), that.getRoleID(), 
	    		new ArrayList<>(that.getDays()), that.getAddress(), that.getDateOfBirth(), that.getPhoneNumber(),
	    		that.getEmailAddress(), that.userCredentials);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getAvatarURL() {
		return avatarURL;
	}
	
	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}
	
	public Integer getRoleID() {
		return roleID;
	}

	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}

	public List<DayDTO> getDays() {
		return days;
	}
	public void setDays(List<DayDTO> days) {
		this.days = days;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public UserCredentialsAddUserDTO getUserCredentials() {
		return userCredentials;
	}

	public void setUserCredentials(UserCredentialsAddUserDTO userCredentials) {
		this.userCredentials = userCredentials;
	}

	
	
}