package com.ttime.dto;

import java.util.Date;
import java.util.List;

public class ScheduleDTO {
	
	private int userID;
	private List<Date> dates;
	private String status;
	private List<WorkHoursDTO> workHours;
	private List<BreakDTO> breaks;
	
	public ScheduleDTO() {
	}

	public ScheduleDTO(int userID, List<Date> dates, String status, List<WorkHoursDTO> workHours,
			List<BreakDTO> breaks) {
		super(); 
		this.dates = dates;
		this.status = status;
		this.workHours = workHours; 
		this.breaks = breaks;
	}
	
	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public List<Date> getDates() {
		return dates;
	}

	public void setDates(List<Date> dates) {
		this.dates = dates;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<WorkHoursDTO> getWorkHours() {
		return workHours;
	}

	public void setWorkHours(List<WorkHoursDTO> workHours) {
		this.workHours = workHours;
	}

	public List<BreakDTO> getBreaks() {
		return breaks;
	}

	public void setBreaks(List<BreakDTO> breaks) {
		this.breaks = breaks;
	} 

}
