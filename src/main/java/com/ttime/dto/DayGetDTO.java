package com.ttime.dto;

import java.util.Date;
import java.util.List;

public class DayGetDTO implements Comparable<DayGetDTO>{
	
	private Date date;
	private List<WorkHoursGetDTO> workHours;
	private List<BreakDTO> breaks;
	private int person_id;
	
	public DayGetDTO(Date date, List<WorkHoursGetDTO> workHours, List<BreakDTO> breaks, int person_id) {
		this.date = date;
		this.workHours = workHours;
		this.breaks = breaks;
		this.person_id = person_id;
	}

	public DayGetDTO() {
	}
	
	public List<WorkHoursGetDTO> getWorkHours() {
		return workHours;
	}

	public void setWorkHours(List<WorkHoursGetDTO> workHours) {
		this.workHours = workHours;
	}

	public int getPerson_id() {
		return person_id;
	}

	public void setPerson_id(int person_id) {
		this.person_id = person_id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<BreakDTO> getBreaks() {
		return breaks;
	}

	public void setBreaks(List<BreakDTO> breaks) {
		this.breaks = breaks;
	}

	@Override
	public int compareTo(DayGetDTO compareDayGetDTO) {
		return getDate().compareTo(compareDayGetDTO.getDate());
	}
	
}