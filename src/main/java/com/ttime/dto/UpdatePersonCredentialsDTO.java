package com.ttime.dto;

public class UpdatePersonCredentialsDTO {
	
	private int id;
	private UpdateUserCredentialsDTO userCredentials;

	
	public UpdatePersonCredentialsDTO(int id, UpdateUserCredentialsDTO userCredentials) {
		this.id = id;
		this.userCredentials = userCredentials;
	}

	public UpdatePersonCredentialsDTO() {
	}
	
	public UpdatePersonCredentialsDTO(UpdatePersonCredentialsDTO that) {
	    this(that.getId(), that.userCredentials);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UpdateUserCredentialsDTO getUserCredentials() {
		return userCredentials;
	}

	public void setUserCredentials(UpdateUserCredentialsDTO userCredentials) {
		this.userCredentials = userCredentials;
	}
	
	
}