package com.ttime.dto;

import java.util.Date;

public class WorkHoursDTO {
	
	private Integer statusId;
	private Date fromHour;
	private Date toHour;
	
	public WorkHoursDTO(Integer statusId, Date fromHour, Date toHour) {
		this.fromHour = fromHour;
		this.toHour = toHour;
		this.statusId = statusId;
	}
	
	public WorkHoursDTO() {
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Date getFromHour() {
		return fromHour;
	}

	public void setFromHour(Date fromHour) {
		this.fromHour = fromHour;
	}

	public Date getToHour() {
		return toHour;
	}

	public void setToHour(Date toHour) {
		this.toHour = toHour;
	}
	
}
