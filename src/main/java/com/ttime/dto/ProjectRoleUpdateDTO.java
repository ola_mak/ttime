package com.ttime.dto;

public class ProjectRoleUpdateDTO {
	
	private int id;
	private String role;
	
	public ProjectRoleUpdateDTO(int id, String role) {
		this.id = id;
		this.role = role;
	}
	
	public ProjectRoleUpdateDTO(String role) {
		this.role = role;
	}

	public ProjectRoleUpdateDTO() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
}
