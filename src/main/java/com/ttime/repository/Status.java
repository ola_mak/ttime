package com.ttime.repository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Status {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String status;
	
	public Status(int id, String status) {
		this.id = id;
		this.status = status;
	}
	
	public Status(String status) {
		this.status = status;
	}

	public Status() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
