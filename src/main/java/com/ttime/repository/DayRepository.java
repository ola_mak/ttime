package com.ttime.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface DayRepository extends CrudRepository<Day, Long>  {
	
	@Query("SELECT u.days FROM Person u WHERE u.id = :person_id")
	List<Day> findAllDayByUserID(@Param("person_id") int person_id);
	
}
