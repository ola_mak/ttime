package com.ttime.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface PersonRepository extends CrudRepository<Person, Long> {
	
	List<Person> findAll();
	
	Person findById(int id);

	@Query("SELECT u FROM Person u WHERE u.userCredentials.username = :username")
	Person findByUsername(@Param("username") String username);
	
	@Transactional 
	@Modifying
	@Query("update Person u set u.name = :name, u.surname = :surname, u.avatarURL = :avatarURL, u.role = :role, u.dateOfBirth = :dateOfBirth, u.address = :address, u.phoneNumber = :phoneNumber, u.emailAddress = :emailAddress where u.id = :id")
	void updatePerson(@Param("name") String name, @Param("surname") String surname, @Param("avatarURL") String avatarURL, @Param("role") ProjectRole role, 
			@Param("dateOfBirth") Date dateOfBirth, @Param("address") Address address, @Param("phoneNumber") String phoneNumber, @Param("emailAddress") String emailAddress, @Param("id") int id);
	
	@Transactional 
	@Modifying
	@Query("update Person u set u.days = :days where u.id = :id")
	void updatePerson(@Param("days") Set<Day> days, @Param("id") int id);
	
	
}

