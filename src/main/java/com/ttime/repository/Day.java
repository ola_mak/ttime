package com.ttime.repository;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Day {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private Date date;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<WorkHours> workHours;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Break> breaks;
	@JoinColumn(name = "person_id")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	Person person;
	
	public Day(Date date, Set<WorkHours> workHours, Set<Break> breaks, Person person) {
		this.date = date;
		this.workHours = workHours;
		this.breaks = breaks;
		this.person = person;
	}
	
	public Day(Date date, Set<WorkHours> workHours, Set<Break> breaks) {
		this.date = date;
		this.breaks = breaks;
		this.workHours = workHours;
	}

	public Day() {
	}
	
	public Set<WorkHours> getWorkHours() {
		return workHours;
	}

	public void setWorkHours(Set<WorkHours> workHours) {
		this.workHours = workHours;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Set<Break> getBreaks() {
		return breaks;
	}

	public void setBreaks(Set<Break> breaks) {
		this.breaks = breaks;
	}
	
    public void addBreak(Break breaks) {
    	breaks.setDay(this);
        this.breaks.add(breaks);
    }
    
    public void addWorkHours(WorkHours workhours) {
    	workhours.setDay(this);
    	this.workHours.add(workhours);
    }

}