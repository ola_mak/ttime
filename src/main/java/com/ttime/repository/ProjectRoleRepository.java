package com.ttime.repository;

import org.springframework.data.repository.CrudRepository;

public interface ProjectRoleRepository extends CrudRepository<ProjectRole, Long> {
	
	ProjectRole findById(int id);

}
