package com.ttime.repository;

import org.springframework.data.repository.CrudRepository;

public interface BreakRepository  extends CrudRepository<Break, Long> {

}
