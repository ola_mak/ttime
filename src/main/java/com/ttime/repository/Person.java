package com.ttime.repository;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Person {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private String surname;
	private String avatarURL;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "projectRole_id")
	private ProjectRole role;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Day> days;	
	@OneToOne(cascade = CascadeType.ALL)
	private Address address;
	private Date dateOfBirth;
	private String phoneNumber;
	private String emailAddress;
	@OneToOne(cascade = CascadeType.ALL)
	private UserCredentials userCredentials;

	
	public Person(int id, String name, String surname, String avatarURL, ProjectRole role, Set<Day> days, 
			Address address, Date dateOfBirth, String phoneNumber, String emailAddress, UserCredentials userCredentials) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.avatarURL = avatarURL;
		this.role = role;
		this.days = days;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
		this.userCredentials = userCredentials;
	}
	
	public Person(Set<Day> days, Address address, UserCredentials userCredentials) {
		this.days = days;
		this.address = address;
		this.userCredentials = userCredentials;
	}

	public Person() {
	}
	
	public Person(Person that) {
	    this(that.getId(), that.getName(), that.getSurname(), that.getAvatarURL(), that.getRole(), 
	    		new HashSet<>(that.getDays()), that.getAddress(), that.getDateOfBirth(), that.getPhoneNumber(),
	    		that.getEmailAddress(), that.getUserCredentials());
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getAvatarURL() {
		return avatarURL;
	}
	
	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}
	
	public ProjectRole getRole() {
		return role;
	}

	public void setRole(ProjectRole role) {
		this.role = role;
	}

	public Set<Day> getDays() {
		return days;
	}
	
	public void setDays(Set<Day> days) {
		this.days = days;
	}
	
    public void addDays(Day day) {
        day.setPerson(this);
        this.days.add(day);
    }

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public UserCredentials getUserCredentials() {
		return userCredentials;
	}

	public void setUserCredentials(UserCredentials userCredentials) {
		this.userCredentials = userCredentials;
	}
		
}