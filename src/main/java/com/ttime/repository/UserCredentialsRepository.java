package com.ttime.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserCredentialsRepository extends CrudRepository<UserCredentials, Long> {
	
	@Query("SELECT u FROM UserCredentials u WHERE u.username = :username")
	UserCredentials findByUsername(@Param("username") String username);
	
	UserCredentials findById(int id);
	
	Iterable<UserCredentials> findAll();

}
