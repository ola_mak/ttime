package com.ttime.repository;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Break {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private Date fromHour;
	private Date toHour;
	@JoinColumn(name = "day_id")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	Day day;
	
	@Override
	public String toString() {
		return "Break [id=" + id + ", fromHour=" + fromHour + ", toHour=" + toHour + ", day=" + day + "]";
	}

	public Break(Date fromHour, Date toHour) {
		this.fromHour = fromHour;
		this.toHour = toHour;
	}
	
	public Break() {
	}
	
	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public Date getFromHour() {
		return fromHour;
	}

	public void setFromHour(Date fromHour) {
		this.fromHour = fromHour;
	}

	public Date getToHour() {
		return toHour;
	}

	public void setToHour(Date toHour) {
		this.toHour = toHour;
	}

}
