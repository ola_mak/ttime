package com.ttime.repository;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class WorkHours {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "status_id")
	private Status status;
	private Date fromHour;
	private Date toHour;
	@JoinColumn(name = "day_id")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	Day day;
	
	
	public WorkHours() {
	}

	public WorkHours(Status status, Date fromHour, Date toHour) {
		this.status = status;
		this.fromHour = fromHour;
		this.toHour = toHour;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getFromHour() {
		return fromHour;
	}
	
	public void setFromHour(Date fromHour) {
		this.fromHour = fromHour;
	}
	
	public Date getToHour() {
		return toHour;
	}
	
	public void setToHour(Date toHour) {
		this.toHour = toHour;
	}
	
	public Day getDay() {
		return day;
	}
	
	public void setDay(Day day) {
		this.day = day;
	}
}