package com.ttime.repository;

import org.springframework.data.repository.CrudRepository;

public interface StatusRepository extends CrudRepository<Status, Long> {
	
	Status findById(int id);

}
