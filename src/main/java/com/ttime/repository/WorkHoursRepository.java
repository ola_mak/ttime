package com.ttime.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface WorkHoursRepository extends CrudRepository<WorkHours, Long> {
	
	List<WorkHours> findAll();
	
}
