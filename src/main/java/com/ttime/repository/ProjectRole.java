package com.ttime.repository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class ProjectRole {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String role;
	
	public ProjectRole(int id, String role) {
		this.id = id;
		this.role = role;
	}
	
	public ProjectRole(String role) {
		this.role = role;
	}

	public ProjectRole() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
}
