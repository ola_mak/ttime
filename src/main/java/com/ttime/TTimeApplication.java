package com.ttime;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TTimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TTimeApplication.class, args);
	}
}
