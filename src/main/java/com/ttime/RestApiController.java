package com.ttime;


import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ttime.dto.AddressDTO;
import com.ttime.dto.DayDTO;
import com.ttime.dto.PersonAddUserDTO;
import com.ttime.dto.PersonDTO;
import com.ttime.dto.PersonUpdateUserDTO;
import com.ttime.dto.ProjectRoleDTO;
import com.ttime.dto.ProjectRoleUpdateDTO;
import com.ttime.dto.ScheduleDTO;
import com.ttime.dto.StatusDTO;
import com.ttime.dto.StatusUpdateDTO;
import com.ttime.dto.UpdatePersonCredentialsDTO;
import com.ttime.dto.UserCredentialsAddUserDTO;
import com.ttime.repository.ProjectRole;
import com.ttime.repository.Role;
import com.ttime.repository.Status;
import com.ttime.service.DayService;
import com.ttime.service.PersonService;
import com.ttime.service.ProjectRoleService;
import com.ttime.service.StatusService;
import com.ttime.service.UserCredentialsService;


@RestController
@RequestMapping(path="/api")
public class RestApiController{
	
	@Autowired
	private PersonService personService;
	@Autowired
	private ProjectRoleService projectRoleService;
	@Autowired
	private StatusService statusService;
	@Autowired
	private UserCredentialsService userCredentialsService;
	@Autowired
	private DayService dayService;
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");	
	
	@Bean
	public Jackson2ObjectMapperBuilderCustomizer jacksonObjectMapperCustomization() {
	    return jacksonObjectMapperBuilder -> 
	        jacksonObjectMapperBuilder.timeZone(TimeZone.getDefault());
	}
	
	 @RequestMapping(path="/save")
	    public void process() throws ParseException{
			personService.addUser(new PersonAddUserDTO("Marcin", "Sroczyński",  "http://marcinsroczynski.com/data/images/person_mock3.jpg", 
					new ArrayList<DayDTO>(), new AddressDTO("Wichrowe Wzgórze", "Poznań", "Wielkopolskie", "61-677", "Polska"), dateFormat.parse("03/12/1995"), "566888999", 
					"marcin.sro@gmail.com", new UserCredentialsAddUserDTO(Role.admin, "us", "ps1234", false)));
	    }

	@RequestMapping(path="/schedules"
			, method = RequestMethod.GET)
	@ResponseBody
	public List<PersonDTO> getSchedules(@RequestParam(name = "date", required = true) String date) throws ParseException, UnsupportedEncodingException {	
		return personService.getSchedulesFromOneWeek(date);
	}	
	
	@RequestMapping(path="/weekSchedules"
			, method = RequestMethod.GET)
	@ResponseBody
	public List<PersonDTO> getSchedulesWithWeekend(@RequestParam(name = "date", required = true) String date) throws ParseException, UnsupportedEncodingException {	
		return personService.getSchedulesWithWeekendFromOneWeek(date);
	}	
	

	@RequestMapping(path="/users"
			, method = RequestMethod.GET)
	@ResponseBody
	public List<PersonDTO> getUsers() throws ParseException, UnsupportedEncodingException {
		return personService.getUsers();
	}	

	@RequestMapping(method = RequestMethod.PUT
            , path = "/schedule"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void editSchedule(@RequestBody @Valid ScheduleDTO request) {
		dayService.editSchedule(request);
	}
	
	@RequestMapping(method = RequestMethod.PUT
            , path = "/user"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateUser(@RequestBody @Valid PersonUpdateUserDTO request) {
		personService.updateUser(request);
	}
	
	@RequestMapping(method = RequestMethod.POST
            , path = "/user"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void addUser(@RequestBody @Valid PersonAddUserDTO request) {
		personService.addUser(request);
	}
	
	@RequestMapping(method = RequestMethod.POST
            , path = "/projectRoles"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void addProjectRoles(@RequestBody @Valid ProjectRoleDTO request) {
		projectRoleService.addProjectRoles(request);
	}
	
	@RequestMapping(method = RequestMethod.PUT
            , path = "/projectRole"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateProjectRole(@RequestBody @Valid ProjectRoleUpdateDTO request) {
		projectRoleService.updateProjectRole(request);
	}
	
	@RequestMapping(path="/projectRoles"
			, method = RequestMethod.GET)
	@ResponseBody
	public List<ProjectRole> getprojectRoles() throws ParseException, UnsupportedEncodingException {
		return projectRoleService.getProjectRoles();
	}
	
	@RequestMapping(path="/projectRole/{id}"
			, method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteProjectRole(@PathVariable int id) throws ParseException, UnsupportedEncodingException {
		projectRoleService.deleteProjectRole(id);
	}
	
	@RequestMapping(method = RequestMethod.POST
            , path = "/statuses"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void addStatuses(@RequestBody @Valid StatusDTO request) {
		statusService.addStatuses(request);
	}
	
	@RequestMapping(method = RequestMethod.PUT
            , path = "/status"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateStatus(@RequestBody @Valid StatusUpdateDTO request) {
		statusService.updateStatus(request);
	}
	
	@RequestMapping(path="/statuses"
			, method = RequestMethod.GET)
	@ResponseBody
	public List<Status> getStatuses() throws ParseException, UnsupportedEncodingException {
		return statusService.getStatuses();
	}
	
	@RequestMapping(path="/status/{id}"
			, method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteStatus(@PathVariable int id) throws ParseException, UnsupportedEncodingException {
		statusService.deleteStatus(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT
            , path = "/userCreds"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateUserCredentials(@RequestBody @Valid UpdatePersonCredentialsDTO request) {
		userCredentialsService.updateUserCredentials(request);
	}
	
	@RequestMapping(method = RequestMethod.PUT
            , path = "/pass"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updatePassword(@RequestBody @Valid UpdatePersonCredentialsDTO request) {
		userCredentialsService.updatePassword(request);
	}
	
	@RequestMapping(method = RequestMethod.PUT
            , path = "/role"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateRole(@RequestBody @Valid UpdatePersonCredentialsDTO request) {
		userCredentialsService.updateRole(request);
	}
	
	@RequestMapping(method = RequestMethod.PUT
            , path = "/username"
            , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateUsername(@RequestBody @Valid UpdatePersonCredentialsDTO request) {
		userCredentialsService.updateUsername(request);
	}
	
	@RequestMapping(path="/user/{id}"
			, method = RequestMethod.GET)
	@ResponseBody
	public PersonDTO getUser(@PathVariable int id) throws ParseException, UnsupportedEncodingException {
		return personService.getUser(id);
	}	
	
	@RequestMapping(path="/user/{id}"
			, method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUser(@PathVariable int id) throws ParseException, UnsupportedEncodingException {
		personService.deleteUser(id);
	}
	
	@RequestMapping(path="/user"
			, method = RequestMethod.GET)
	@ResponseBody
	public PersonDTO getUser(Authentication authentication,Principal principal) throws ParseException, UnsupportedEncodingException {
		return personService.getUserByUsername(authentication.getName());
	}
	
}