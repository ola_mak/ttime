package com.ttime.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ttime.configuration.LDAPConfiguration;
import com.ttime.dto.Convert;
import com.ttime.dto.DayGetDTO;
import com.ttime.dto.PersonAddUserDTO;
import com.ttime.dto.PersonDTO;
import com.ttime.dto.PersonUpdateUserDTO;
import com.ttime.repository.Address;
import com.ttime.repository.AddressRepository;
import com.ttime.repository.Person;
import com.ttime.repository.PersonRepository;
import com.ttime.repository.ProjectRole;
import com.ttime.repository.UserCredentials;
import com.ttime.repository.UserCredentialsRepository;

public class PersonService {

	@Autowired
	PersonRepository personRepository;
	@Autowired
	AddressRepository addressRepository;
	@Autowired
	UserCredentialsRepository userCredentialsRepository;
	@Autowired
	UserCredentialsService userCredentialsService; 
	@Autowired
	ProjectRoleService projectRoleService;
	@Autowired
	Convert convert;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public PersonDTO getUser(int id) {
		Person user = personRepository.findById(id);
		return convert.convertPersonToDto(user);
	}
	
	public PersonDTO getUserByUsername(String username) {
		Person user = personRepository.findByUsername(username);
		return convert.convertPersonToDto(user);
	}

	public List<PersonDTO> getUsers() {

		List<Person> usersFromDB = personRepository.findAll();
		List<PersonDTO> users = new ArrayList<>();

		for (Person person : usersFromDB) {
			users.add(convert.convertPersonToDto(person));
		}
		return users;
	}

	public boolean isWithinWeek(String date, Date dateFromSchedule) throws ParseException {

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		calendar.setTime(dateFormat.parse(date));
		if (calendar.DAY_OF_WEEK == 7 || calendar.DAY_OF_WEEK == 1) {
			calendar.add(Calendar.DATE, 2);
		}
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		Date startDate = calendar.getTime();
		calendar.add(Calendar.DATE, 6);
		Date endDate = calendar.getTime();

		Calendar calendarForDateToCheck = Calendar.getInstance();
		calendarForDateToCheck.setTimeInMillis(dateFromSchedule.getTime());
		Date dateToCheck = calendarForDateToCheck.getTime();

		boolean isAfter = dateToCheck.after(startDate) || dateToCheck.equals(startDate);
		boolean isBefore = dateToCheck.before(endDate) || dateToCheck.equals(endDate);

		return isAfter && isBefore;

	}
	
	public boolean isWithinWeekWithWeekends(String date, Date dateFromSchedule) throws ParseException {

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		calendar.setTime(dateFormat.parse(date));
		
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		Date startDate = calendar.getTime();
		calendar.add(Calendar.DATE, 6);
		Date endDate = calendar.getTime();

		Calendar calendarForDateToCheck = Calendar.getInstance();
		calendarForDateToCheck.setTimeInMillis(dateFromSchedule.getTime());
		Date dateToCheck = calendarForDateToCheck.getTime();

		boolean isAfter = dateToCheck.after(startDate) || dateToCheck.equals(startDate);
		boolean isBefore = dateToCheck.before(endDate) || dateToCheck.equals(endDate);

		return isAfter && isBefore;

	}

	public List<PersonDTO> getSchedulesFromOneWeek(String date) throws ParseException {

		List<PersonDTO> schedulesFromOneWeek = getUsers();

		for (int i = 0; i < schedulesFromOneWeek.size(); i++) {
			List<DayGetDTO> days = schedulesFromOneWeek.get(i).getDays();
			int daysSize = days.size();
			for (int j = 0; j < daysSize; j++) {
				Date dateFromSchedule = days.get(j).getDate();
				if (!isWithinWeek(date, dateFromSchedule)) {
					schedulesFromOneWeek.get(i).getDays().remove(days.get(j));
					daysSize--;
					j--;
				}
			}
			Collections.sort(days);
		}

		return schedulesFromOneWeek;
	}
	
	public List<PersonDTO> getSchedulesWithWeekendFromOneWeek(String date) throws ParseException {
		List<PersonDTO> schedulesFromOneWeek = getUsers();

		for (int i = 0; i < schedulesFromOneWeek.size(); i++) {
			List<DayGetDTO> days = schedulesFromOneWeek.get(i).getDays();
			int daysSize = days.size();
			for (int j = 0; j < daysSize; j++) {
				Date dateFromSchedule = days.get(j).getDate();
				if (!isWithinWeekWithWeekends(date, dateFromSchedule)) {
					schedulesFromOneWeek.get(i).getDays().remove(days.get(j));
					daysSize--;
					j--;
				}
			}
			Collections.sort(days);
		}

		return schedulesFromOneWeek;
	}
	
	public void updateUser(PersonUpdateUserDTO request) {
		int id = request.getId();
		String name = request.getName();
		String surname = request.getSurname();
		String avatarURL = request.getAvatarURL();
		Integer projectRoleId = request.getRoleID();
		ProjectRole projectRole = null;
		if (projectRoleId != null) {
			projectRole = projectRoleService.findRoleById(projectRoleId);
			validateRole(projectRoleId);
		}
		
		Date dateOfBirth = request.getDateOfBirth();
		Address address = convert.convertAddressToEntity(request.getAddress());
		String phoneNumber = request.getPhoneNumber();
		String emailAddress = request.getEmailAddress();
		
		
		addressRepository.save(address);
		personRepository.updatePerson(name, surname, avatarURL, projectRole, dateOfBirth, address, phoneNumber, emailAddress, id);
	}

	private void validateRole(Integer projectRoleId) {
		List<Integer> allProjectRolesIds  = projectRoleService.getAllRolesIds();
		if (!allProjectRolesIds.contains(projectRoleId)) {
			throw new IllegalArgumentException("Role with given id does not exist");
		}
	}

	public void addUser(PersonAddUserDTO request) {
		Person person = convert.convertPersonToEntity(request);
		UserCredentials userCredentialsFromRequest = convert.convertUserCredentialsToEntity(request.getUserCredentials());
		Address address = convert.convertAddressToEntity(request.getAddress());
		ProjectRole projectRole = null;
		if (request.getRoleID() != null)
		{
			projectRole = projectRoleService.findRoleById(request.getRoleID());
			validateRole(request.getRoleID());
		} 
		person.setRole(projectRole);
		person.setAddress(address);
		person.setUserCredentials(userCredentialsFromRequest);
		Set<String> dbUsernames = userCredentialsService.getUsernames();
		String username = userCredentialsFromRequest.getUsername();
		String password = userCredentialsFromRequest.getPassword();
		boolean authenticated;
		String pass = null;
		
		
		
		if (userCredentialsFromRequest.isLdap()) {
			authenticated = LDAPConfiguration.checkUserCredentials(username, password);
			if (authenticated) {
				savePerson(person, userCredentialsFromRequest, dbUsernames, username, pass);
			} else {
				throw new IllegalArgumentException("User does not exist in LDAP or there was connection problem");
			}
		} else {
			pass = bCryptPasswordEncoder.encode(userCredentialsFromRequest.getPassword());
			savePerson(person, userCredentialsFromRequest, dbUsernames, username, pass);
		}
	}

	private void savePerson(Person person, UserCredentials userCredentialsFromRequest, Set<String> dbUsernames,
			String username, String pass) {
		if (!dbUsernames.contains(username)) {
			userCredentialsFromRequest.setPassword(pass);
			userCredentialsRepository.save(userCredentialsFromRequest);
			personRepository.save(person);
		} else {
			throw new IllegalArgumentException("Username " + username + " is already used");
		}
	}
	
	public void deleteUser(int id) {
		Person person = personRepository.findById(id);
		personRepository.delete(person);
	}

	public boolean obtainUser(String username, String password) {
		String pass = personRepository.findByUsername(username).getUserCredentials().getPassword();
		return bCryptPasswordEncoder.matches(password, pass);
	}
	
	public List<Integer> getUsedProjectRolesIds() {
		List<Person> users = personRepository.findAll();
		List<Integer> projectRolesIds = new ArrayList<>();
		for(Person person : users) {
			if (person.getRole() != null) {
				projectRolesIds.add(person.getRole().getId());
			}
		}
		return projectRolesIds;
	}

}
