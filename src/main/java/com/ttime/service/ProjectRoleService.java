package com.ttime.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ttime.dto.Convert;
import com.ttime.dto.ProjectRoleDTO;
import com.ttime.dto.ProjectRoleUpdateDTO;
import com.ttime.repository.ProjectRole;
import com.ttime.repository.ProjectRoleRepository;

public class ProjectRoleService {
	
	@Autowired
	ProjectRoleRepository projectRoleRepository;
	@Autowired 
	PersonService personService;
	@Autowired
	Convert convert;
	
	public ProjectRole findRoleById(int id) {
		ProjectRole role = projectRoleRepository.findById(id);
		return role;
	}
	
	private Boolean isRoleNew(String role) {
		
		List<ProjectRole> projectRolesFromDB = (List<ProjectRole>) projectRoleRepository.findAll();
		List<String> rolesFromDB = new ArrayList<>();
		for (ProjectRole projectRoleFromDB : projectRolesFromDB) {
			rolesFromDB.add(projectRoleFromDB.getRole());
		}
			
		return !rolesFromDB.contains(role);
	}
	
	public void addProjectRoles(ProjectRoleDTO request) {
		List<String> roles = request.getRoles();
		
		for (String role : roles) {
			if (isRoleNew(role)) {
				ProjectRole projectRole = new ProjectRole(role);
				projectRoleRepository.save(projectRole);
			}
		}
	}
	
	public void updateProjectRole(ProjectRoleUpdateDTO request) {
		projectRoleRepository.save(convert.convertProjectRoleToEntity(request));
	}
	
	public List<ProjectRole> getProjectRoles() {
		List<ProjectRole> projectRoles = (List<ProjectRole>) projectRoleRepository.findAll();
		return projectRoles;
	}
	
	public void deleteProjectRole(int id) {
		List<Integer> usedProjectRolesIds = personService.getUsedProjectRolesIds();
		ProjectRole projectRole = projectRoleRepository.findById(id);
		Integer idObj = new Integer(id);
		
		if(!usedProjectRolesIds.contains(idObj)) {
			projectRoleRepository.delete(projectRole);
		} else {
			throw new IllegalArgumentException("Cannot delete role that is being used in users profiles");
		}
	}
	
	public List<Integer> getAllRolesIds() {
		List<ProjectRole> projectRoles = (List<ProjectRole>) projectRoleRepository.findAll();
		List<Integer> projectRolesIds = new ArrayList<>();
		for (ProjectRole projectRole : projectRoles) {
			projectRolesIds.add(projectRole.getId());
		}
		return projectRolesIds;
	}
}
 