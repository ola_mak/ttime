package com.ttime.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.ttime.dto.Convert;
import com.ttime.dto.ScheduleDTO;
import com.ttime.dto.WorkHoursDTO;
import com.ttime.repository.Break;
import com.ttime.repository.BreakRepository;
import com.ttime.repository.Day;
import com.ttime.repository.DayRepository;
import com.ttime.repository.Person;
import com.ttime.repository.PersonRepository;
import com.ttime.repository.WorkHours;

public class DayService {
	
	@Autowired
	DayRepository dayRepository;
	@Autowired
	PersonRepository personRepository; 
	@Autowired
	BreakRepository breakRepository;
	@Autowired
	StatusService statusService;
	@Autowired
	Convert convert;
	
	public Date convertDate(Date dateToConvert) {
		Calendar calendarForDateToConvert = Calendar.getInstance();
		calendarForDateToConvert.setTimeInMillis(dateToConvert.getTime());
		Date date = calendarForDateToConvert.getTime();
		return date;
	}
	
	public void editSchedule(ScheduleDTO request) {
		
		Person person = personRepository.findById(request.getUserID());
		//possible npe when incorrect id
		Set<Day> daysofPerson = person.getDays();
		Set<Day> daysToSave = new HashSet<>();
		
		List<Date> datesFromRequest = request.getDates();
		List<Date> existingDates = getExistingDates(daysofPerson);
		
		validateStatusId(request);
		
		for (Date dateFromRequest : datesFromRequest) {
			if (scheduleExistsForDateFromRequest(dateFromRequest, existingDates)) {
				updateExistingDay(request, person, daysofPerson, daysToSave, dateFromRequest);
			} else {
				addDayForNewDate(request, person, dateFromRequest);
			}		
		}
		
		saveDay(daysToSave);
		savePerson(person);
	}

	private void validateStatusId(ScheduleDTO request) {
		List<Integer> allStatusesIds  = statusService.getAllStatusesIds();
		List<WorkHoursDTO> workHours =  request.getWorkHours();
		for (WorkHoursDTO workHour : workHours) {
			if(!allStatusesIds.contains(workHour.getStatusId())) {
				throw new IllegalArgumentException("Status with given id does not exist");
			}	
		}
	}
	
	private boolean scheduleExistsForDateFromRequest(Date dateFromRequest, List<Date> existingDates) {
		return existingDates.contains(dateFromRequest);
	}

	private List<Date> getExistingDates(Set<Day> daysofPerson) {
		List<Date> existingDates = new ArrayList<>();
		
		for (Day day : daysofPerson) {
			existingDates.add(convertDate(day.getDate()));
		}
		return existingDates;
	}

	private void savePerson(Person person) {
		personRepository.save(person);
	}

	private void saveDay(Set<Day> daysToSave) {
		for (Day day : daysToSave) {
			dayRepository.save(day);
		}
	}

	private void addDayForNewDate(ScheduleDTO request, Person person, Date dateFromRequest) {
		Set<Break> breaks = convert.convertBreaksToEntity(request.getBreaks());
		Set<WorkHours> workHours = convert.convertWorkHoursToEntity(request.getWorkHours());
		Day day = new Day(dateFromRequest, workHours, breaks);
		for (Break oneBreak : breaks) {
			day.addBreak(oneBreak);
		}
		for (WorkHours workHour : workHours) {
			day.addWorkHours(workHour);
		}
		person.addDays(day);
	}

	private void updateExistingDay(ScheduleDTO request, Person person, Set<Day> daysofPerson, Set<Day> daysToSave, Date dateFromRequest) {
		for (Day  day : daysofPerson) {
			if (convertDate(day.getDate()).equals(dateFromRequest)) {
				day.setWorkHours(convert.convertWorkHoursToEntity(request.getWorkHours()));
				day.setBreaks(convert.convertBreaksToEntity(request.getBreaks()));
				day.setPerson(person);
				daysToSave.add(day);
			}
		}
	}


}
