package com.ttime.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.ttime.dto.PersonDTO;
import com.ttime.dto.ScheduleDTO;

public interface ScheduleService {
	List<PersonDTO> obtainSchedules() throws ParseException;
	boolean isWithinWeek(String date, Date dateToCheck) throws ParseException;
	List<PersonDTO> obtainSchedulesFromOneWeek(List<PersonDTO> schedules, String date) throws ParseException;
	void addSchedule(ScheduleDTO request);
}
