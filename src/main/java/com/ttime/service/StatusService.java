package com.ttime.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ttime.dto.Convert;
import com.ttime.dto.StatusDTO;
import com.ttime.dto.StatusUpdateDTO;
import com.ttime.repository.Status;
import com.ttime.repository.StatusRepository;

public class StatusService {
	
	@Autowired
	StatusRepository statusRepository;
	@Autowired
	WorkHoursService workHoursService;
	@Autowired
	Convert convert;
	
	
	private Boolean isStatusNew(String status) {
		
		List<Status> statuses = (List<Status>) statusRepository.findAll();
		List<String> statusesList = getStatusesList(statuses);
		
		
		return !statusesList.contains(status);
	}

	private List<String> getStatusesList(List<Status> statuses) {
		List<String> statusesList = new ArrayList<>();
		for (Status s : statuses) {
			statusesList.add(s.getStatus());
		}
		return statusesList;
	}
	
	public List<Integer> getAllStatusesIds() {
		List<Status> statuses = (List<Status>) statusRepository.findAll();
		List<Integer> statusesIds = new ArrayList<>();
		for (Status status : statuses) {
			statusesIds.add(status.getId());
		}
		return statusesIds;
	}
	
	public void addStatuses(StatusDTO request) {
		List<String> statuses = request.getStatuses();
		for (String status : statuses) {
			if(isStatusNew(status)) {
				Status newStatus = new Status(status);
				statusRepository.save(newStatus);
			}
		}
	}
	
	public void updateStatus(StatusUpdateDTO request) {

		statusRepository.save(convert.convertStatusToEntity(request));
	}
	
	public List<Status> getStatuses() {
		List<Status> statuses = (List<Status>) statusRepository.findAll();
		return statuses;
	}
	
	public void deleteStatus(int id) {
		List<Integer> statusesIds = workHoursService.getUsedStatusesIds();
		
		Integer idObj = new Integer(id);

		if (!statusesIds.contains(idObj)) {
			Status status = statusRepository.findById(id);
			statusRepository.delete(status);
		} else {
			throw new IllegalArgumentException("Cannot delete status that is being used in users schedules");
		}
		
	}

}
 
