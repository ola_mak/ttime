package com.ttime.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ttime.repository.WorkHours;
import com.ttime.repository.WorkHoursRepository;

public class WorkHoursService {
	
	@Autowired 
	WorkHoursRepository workHoursRepository;
	
	List<Integer> getUsedStatusesIds() {
		List<WorkHours> workHours = workHoursRepository.findAll();
		List<Integer> statusIds = new ArrayList<>();
		for(WorkHours workHour : workHours) {
			if (workHour.getStatus() != null) {
				statusIds.add(workHour.getStatus().getId());
			}
		}
		return statusIds;
	}
}
