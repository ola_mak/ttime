package com.ttime.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ttime.dto.Convert;
import com.ttime.dto.UpdatePersonCredentialsDTO;
import com.ttime.repository.AddressRepository;
import com.ttime.repository.Person;
import com.ttime.repository.PersonRepository;
import com.ttime.repository.Role;
import com.ttime.repository.UserCredentials;
import com.ttime.repository.UserCredentialsRepository;

public class UserCredentialsService {
	
	@Autowired
	PersonRepository personRepository;
	@Autowired
	AddressRepository addressRepository;
	@Autowired
	UserCredentialsRepository userCredentialsRepository;
	@Autowired
	Convert convert;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public void updateUserCredentials(UpdatePersonCredentialsDTO request) {
		
		int personID = request.getId();
		Person person = personRepository.findById(personID);
		
		Role role = request.getUserCredentials().getRole();
		String username = request.getUserCredentials().getUsername();
		String password = request.getUserCredentials().getPassword();
		UserCredentials userCredentialsOfPerson = person.getUserCredentials();

		userCredentialsOfPerson.setRole(role);
		userCredentialsOfPerson.setPassword(bCryptPasswordEncoder.encode(password));
		
		Set<String> dbUsernames = getUsernames();
		if (!dbUsernames.contains(username)) {
			userCredentialsOfPerson.setUsername(username);
		} else {
			throw new IllegalArgumentException("Username " + username + " is already used");
		}		
		
		userCredentialsRepository.save(userCredentialsOfPerson);
		personRepository.save(person);		
	}
	
	public Set<String> getUsernames() {
		
		Iterable<UserCredentials> allUserCredentials = userCredentialsRepository.findAll();
		Set<String> usernames = new HashSet<>();
		
		for (UserCredentials userCredential : allUserCredentials) {
			usernames.add(userCredential.getUsername());
		}
		return usernames;
	}

	public void updateUsername(UpdatePersonCredentialsDTO request) {
		
		int personID = request.getId();
		Person person = personRepository.findById(personID);
		String username = request.getUserCredentials().getUsername();
		UserCredentials userCredentialsOfPerson = person.getUserCredentials();
		
		Set<String> dbUsernames = getUsernames();
		if (!dbUsernames.contains(username)) {
			userCredentialsOfPerson.setUsername(username);
			userCredentialsRepository.save(userCredentialsOfPerson);
			personRepository.save(person);
		} else {
			throw new IllegalArgumentException("Username " + username + " is already used");
		}		
	}
	
	public void updateRole(UpdatePersonCredentialsDTO request) {
		
		int personID = request.getId();
		Person person = personRepository.findById(personID);
		Role role = request.getUserCredentials().getRole();
		UserCredentials userCredentialsOfPerson = person.getUserCredentials();

		userCredentialsOfPerson.setRole(role);
		userCredentialsRepository.save(userCredentialsOfPerson);
		personRepository.save(person);		
	}

	public void updatePassword(UpdatePersonCredentialsDTO request) {
		
		int personID = request.getId();
		Person person = personRepository.findById(personID);
		String password = request.getUserCredentials().getPassword();
		UserCredentials userCredentialsOfPerson = person.getUserCredentials();

		userCredentialsOfPerson.setPassword(bCryptPasswordEncoder.encode(password));
		userCredentialsRepository.save(userCredentialsOfPerson);
		personRepository.save(person);		
	}
	
}
