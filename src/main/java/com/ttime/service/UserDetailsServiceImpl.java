package com.ttime.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ttime.repository.UserCredentials;
import com.ttime.repository.UserCredentialsRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
	
	private UserCredentialsRepository userCredentialsRepository;

	
	public UserDetailsServiceImpl(UserCredentialsRepository userCredentialsRepository) {
		this.userCredentialsRepository = userCredentialsRepository;
	}


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserCredentials userCredentials = userCredentialsRepository.findByUsername(username);
		if (userCredentials == null) {
			throw new UsernameNotFoundException(username);
		}
		return new User(userCredentials.getUsername(), userCredentials.getPassword(), getAuthority(userCredentials));
	}
	
	private Set<SimpleGrantedAuthority> getAuthority(UserCredentials userCredentials) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + userCredentials.getRole()));
	
		return authorities;
	}

}
